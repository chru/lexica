@prefix boston: <http://ontosem.net/ontology/BostonHousing#> .

Lexicon(<http://ontosem.net/lexicon/Boston_Housing_en#>,"en",


   //// Classes ////

   ClassNoun("suburb",boston:Suburb),
   ClassNoun("neighbourhood",boston:Suburb),
   ClassNoun("house",boston:House),

   //// House properties ////

   RelationalAdjective("located",boston:location,
      relationalArg = PrepositionalObject("in")),

   RelationalNoun("price",boston:price,
      propSubj = PrepositionalObject("of"),
      propObj  = CopulativeArg)
      with plural "prices", 

   StateVerb("cost",boston:price),

   RelationalAdjective("worth",boston:price,
      relationalArg = DirectObject),


   //// Suburb properties ////

   RelationalNoun("house price",boston:housePrice,
      propSubj = PrepositionalObject("in"),
      propObj  = CopulativeArg) 
      with plural "house prices", 

   RelationalNoun("crime rate",boston:crim,
      propSubj = PrepositionalObject("in"),
      propObj  = CopulativeArg) 
      with plural "crime rates",

   RelationalNoun("nitrogen oxide concentration",boston:nox,
      propSubj = PrepositionalObject("in"),
      propObj  = CopulativeArg),
   RelationalNoun("air pollution",boston:nox,
      propSubj = PrepositionalObject("in"),
      propObj  = CopulativeArg),


   ScalarAdjective("expensive",[boston:housePrice > 10000.0 for boston:Suburb]),
   ScalarAdjective("cheap",[boston:housePrice < 80000.0 for boston:Suburb]),

   ScalarAdjective("safe",[boston:crim < 0.04 for boston:Suburb]),
   ScalarAdjective("unsafe",[boston:crim > 0.1 for boston:Suburb]),
   ScalarAdjective("crime-ridden",[boston:crim > 0.1 for boston:Suburb]),

   ScalarAdjective("polluted",[boston:nox > 0.08 for boston:Suburb]),
   ScalarAdjective("clean",[boston:nox < 0.04 for boston:Suburb]),

   ScalarAdjective("old",[boston:age > 40.0 for boston:Suburb]),
   ScalarAdjective("new",[boston:age < 10.0 for boston:Suburb]),

   ScalarAdjective("easily accessible",[boston:rad > 0.0 for boston:Suburb]),

   
   //// Individuals ////

   Name("Skywalker Ranch",boston:House1),
   Name("Maniac Mansion",boston:House2),
   Name("The Beach House",boston:House3),

   Name("Nahant",boston:Nahant),
   Name("Swampscott",boston:Swampscott), 
   Name("Marblehead",boston:Marblehead), 
   Name("Salem",boston:Salem), 
   Name("Lynn",boston:Lynn), 
   Name("Saugus",boston:Saugus), 
   Name("Lynnfield",boston:Lynnfield), 
   Name("Peabody",boston:Peabody), 
   Name("Danvers",boston:Danvers), 
   Name("Middleton",boston:Middleton), 
   Name("Topsfield",boston:Topsfield), 
   Name("Hamilton",boston:Hamilton), 
   Name("Wenham",boston:Wenham), 
   Name("Beverly",boston:Beverly), 
   Name("Manchester",boston:Manchester), 
   Name("Northreading",boston:Northreading), 
   Name("Wilmington",boston:Wilmington), 
   Name("Burlington",boston:Burlington), 
   Name("Woburn",boston:Woburn), 
   Name("Reading",boston:Reading), 
   Name("Wakefield",boston:Wakefield), 
   Name("Melrose",boston:Melrose), 
   Name("Stoneham",boston:Stoneham), 
   Name("Winchester",boston:Winchester), 
   Name("Medford",boston:Medford), 
   Name("Malden",boston:Malden), 
   Name("Everett",boston:Everett), 
   Name("Somerville",boston:Somerville), 
   Name("Cambridge",boston:Cambridge), 
   Name("Arlington",boston:Arlington), 
   Name("Belmont",boston:Belmont), 
   Name("Lexington",boston:Lexington), 
   Name("Bedford",boston:Bedford), 
   Name("Lincoln",boston:Lincoln), 
   Name("Concord",boston:Concord), 
   Name("Sudbury",boston:Sudbury), 
   Name("Wayland",boston:Wayland), 
   Name("Weston",boston:Weston), 
   Name("Waltham",boston:Waltham), 
   Name("Watertown",boston:Watertown), 
   Name("Newton",boston:Newton), 
   Name("Natick",boston:Natick), 
   Name("Framingham",boston:Framingham), 
   Name("Ashland",boston:Ashland), 
   Name("Sherborn",boston:Sherborn), 
   Name("Brookline",boston:Brookline), 
   Name("Deadham",boston:Dedham), 
   Name("Needham",boston:Needham), 
   Name("Wellesley",boston:Wellesley), 
   Name("Dover",boston:Dover), 
   Name("Medfield",boston:Medfield), 
   Name("Millis",boston:Millis), 
   Name("Norfolk",boston:Norfolk), 
   Name("Walpole",boston:Walpole), 
   Name("Westwood",boston:Westwood), 
   Name("Norwood",boston:Norwood), 
   Name("Sharon",boston:Sharon), 
   Name("Canton",boston:Canton), 
   Name("Milton",boston:Milton), 
   Name("Quincy",boston:Quincy), 
   Name("Braintree",boston:Braintree), 
   Name("Randolph",boston:Randolph), 
   Name("Holbrook",boston:Holbrook), 
   Name("Weymouth",boston:Weymouth), 
   Name("Cohasset",boston:Cohasset), 
   Name("Hull",boston:Hull), 
   Name("Hingham",boston:Hingham), 
   Name("Rockland",boston:Rockland), 
   Name("Hanover",boston:Hanover), 
   Name("Norwell",boston:Norwell), 
   Name("Scituate",boston:Scituate), 
   Name("Marshfield",boston:Marshfield), 
   Name("Duxbury",boston:Duxbury), 
   Name("Pembroke",boston:Pembroke), 
   Name("Allston Brighton",boston:AllstonBrighton), 
   Name("Backbay",boston:Backbay), 
   Name("Beaconhill",boston:Beaconhill), 
   Name("Northend",boston:Northend), 
   Name("Charlestown",boston:Charlestown), 
   Name("East Boston",boston:Eastboston), 
   Name("South Boston",boston:Southboston), 
   Name("Downtown",boston:Downtown), 
   Name("Roxbury",boston:Roxbury), 
   Name("Savinhill",boston:Savinhill), 
   Name("Dorchester",boston:Dorchester), 
   Name("Mattapan",boston:Mattapan), 
   Name("Foresthills",boston:Foresthills), 
   Name("Westroxbury",boston:Westroxbury), 
   Name("Hydepark",boston:Hydepark), 
   Name("Chelsea",boston:Chelsea), 
   Name("Revere",boston:Revere), 
   Name("Winthrop",boston:Winthrop)

)
