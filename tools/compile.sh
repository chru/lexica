#!/bin/bash

if [ "$1" == "travel" ]
then 
    echo "[Flight travel] Compiling English lexicon..."
    cd ../lemon/travel
    lemonpatterns travel_en.ldp travel_en_part.rdf
    python ../../tools/RDFmerger.py "http://sc.cit-ec.uni-bielefeld.de/lexica/travel_en#" travel_en_part.rdf travel_extra_en.ttl ../../target/travel_en.rdf
    rm travel_en_part.rdf

    echo "[Flight travel] Compiling German lexicon..."
    lemonpatterns travel_de.ldp travel_de_part.rdf
    python ../../tools/RDFmerger.py "http://sc.cit-ec.uni-bielefeld.de/lexica/travel_de#" travel_de_part.rdf travel_extra_de.ttl ../../target/travel_de.rdf
    rm travel_de_part.rdf

    echo "[Flight travel] Compiling Dutch lexicon..."
    lemonpatterns travel_nl.ldp travel_nl_part.rdf
    python ../../tools/RDFmerger.py "http://sc.cit-ec.uni-bielefeld.de/lexica/travel_nl#" travel_nl_part.rdf travel_extra_nl.ttl ../../target/travel_nl.rdf
    rm travel_nl_part.rdf
fi 

if [ "$1" == "travel_with_abox" ]
then 
    echo "[Flight travel] Compiling English lexicon..."
    cd ../lemon/travel
    lemonpatterns travel_en.ldp travel_en_part.rdf
    python ../../tools/RDFmerger.py "http://sc.cit-ec.uni-bielefeld.de/lexica/travel_en#" travel_en_part.rdf travel_extra_en.ttl travelDomain_ABox_en.ttl ../../target/travel_en.rdf
    rm travel_en_part.rdf

    echo "[Flight travel] Compiling German lexicon..."
    lemonpatterns travel_de.ldp travel_de_part.rdf
    python ../../tools/RDFmerger.py "http://sc.cit-ec.uni-bielefeld.de/lexica/travel_de#" travel_de_part.rdf travel_extra_de.ttl travelDomain_ABox_de.ttl ../../target/travel_de.rdf
    rm travel_de_part.rdf

    echo "[Flight travel] Compiling Dutch lexicon..."
    lemonpatterns travel_nl.ldp travel_nl_part.rdf
    python ../../tools/RDFmerger.py "http://sc.cit-ec.uni-bielefeld.de/lexica/travel_nl#" travel_nl_part.rdf travel_extra_nl.ttl travelDomain_ABox_nl.ttl ../../target/travel_nl.rdf
    rm travel_nl_part.rdf
fi 


if [ "$1" == "space" ]
then 
    echo "[Space] Compiling English lexicon..."
    cd ../lemon/space
    lemonpatterns space_en.ldp space_en_part.rdf
    python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/space_en#" space_en_part.rdf ontology/space_extra.ttl ../../target/space_en.rdf # space_extra_en.ttl
    rm space_en_part.rdf

    echo "[Space] Compiling Dutch lexicon..."
    lemonpatterns space_nl.ldp space_nl_part.rdf
    python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/space_en#" space_nl_part.rdf ontology/space_extra.ttl ../../target/space_nl.rdf # space_extra_nl.ttl
    rm space_nl_part.rdf
fi

if [ "$1" == "soccer" ]
then 
    echo "[Soccer] Compiling English lexicon..."
    cd ../lemon/soccer
    lemonpatterns soccer_en.ldp soccer_en_part.rdf
    python ../../tools/RDFmerger.py "http://http://ontosem.net/lexicon/soccer_en.rdf#" soccer_en_part.rdf soccer_en_extra.ttl extra.ttl ../../target/soccer_en.rdf
    rm soccer_en_part.rdf
fi 

if [ "$1" == "boston" ]
then 
    echo "[Boston housing] Compiling English lexicon..."
    cd ../lemon/boston
    lemonpatterns Boston_Housing_en.ldp Boston_Housing_en_part.rdf
    python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/Boston_Housing_en#" Boston_Housing_en_part.rdf Boston_Housing_extra_en.ttl ../../target/boston_en.rdf
    rm Boston_Housing_en_part.rdf
fi 

if [ "$1" == "finance" ]
then 
    echo "[Finance] Compiling English lexicon..."
    cd ../lemon/finance
    lemonpatterns Finance_en.ldp ../../target/Finance_en.rdf
    #python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/Boston_Housing_en#" Boston_Housing_en_part.rdf Boston_Housing_extra_en.ttl ../../target/boston_en.rdf
    #rm Boston_Housing_en_part.rdf
fi 


if [ "$1" == "asa" ]
then 
    cd ../lemon/asa
    echo "[ASA] Compiling English lexicon..."
    lemonpatterns ASA_en.ldp ASA_en_part.rdf
    python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/ASA_en#" ASA_en_part.rdf ASA_extra_en.ttl ../../target/ASA_en.rdf
    rm ASA_en_part.rdf
    echo "[ASA] Compiling Dutch lexicon..."
    lemonpatterns ASA_nl.ldp ASA_nl_part.rdf
    python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/ASA_nl#" ASA_nl_part.rdf ASA_extra_nl.ttl ../../target/ASA_nl.rdf
    rm ASA_nl_part.rdf
    echo "[ASA] Compiling German lexicon..."
    lemonpatterns ASA_de.ldp ASA_de_part.rdf
    python ../../tools/RDFmerger.py "http://ontosem.net/lexicon/ASA_de#" ASA_de_part.rdf ASA_extra_de.ttl ../../target/ASA_de.rdf
    rm ASA_de_part.rdf
fi
