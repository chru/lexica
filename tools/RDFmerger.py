
import rdflib as rdf
from   rdflib import Namespace, RDF, URIRef

from sys import argv,exit
from os  import listdir
from os.path import isfile

import re


lexicon = URIRef(argv[1])
lemon   = Namespace("http://lemon-model.net/lemon#")


def frag_uri(uri):
    if not ':/' in str(uri): return str(uri)
    else: return re.match('.*/([^/]+\#)?([^/]+)$',str(uri)).group(2)


def run():

  files = []
  for a in argv[2:-1]:
      if isfile(a):
         files.append(a)
      else: 
         for f in listdir(a):
             if not f.endswith("gitignore") and not f.endswith("~"):
                files.append(a+f)

  graph = rdf.Graph()
  for f in files:
      print 'Loading ' + str(f) + '...'
      if f.endswith('.owl') or f.endswith('.rdf'):
         graph = graph.parse(f)
      elif f.endswith('.ttl') or f.endswith('.n3') or f.endswith('.nt'):
         graph = graph.parse(f,format='n3') 
      else: 
         print 'Unknown format: '+f
         exit(1)

  print 'Merging lexica into <'+str(lexicon)+'>...'

  for l,_,_ in graph.triples((None,RDF.type,lemon.Lexicon)):
      if l != lexicon:
         for s,p,o in graph.triples((l,RDF.type,lemon.Lexicon)):
             graph.remove((s,p,o))
         for s,p,o in graph.triples((l,lemon.language,None)):
             graph.remove((s,p,o))
         for s,p,o in graph.triples((l,lemon.entry,None)):
             graph.remove((s,p,o))
             graph.add((lexicon,p,o))

  print 'Serializing...'

  result_file = argv[-1:][0]

  out = open(result_file,'w')
  out.write(graph.serialize()+'\n\n')
  out.close()

  print 'Done: ' + result_file


## MAIN #########

if __name__ == "__main__":
   run()

